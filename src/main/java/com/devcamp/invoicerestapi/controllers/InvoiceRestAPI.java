package com.devcamp.invoicerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.invoicerestapi.models.InvoiceItem;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceRestAPI {
    @GetMapping("/invoice")
    public  ArrayList<InvoiceItem> Invoice(){
        ArrayList<InvoiceItem>  InvoiceArray = new ArrayList<>();
        InvoiceItem invoiceItem1 = new InvoiceItem("1", "an uong", 8, 5000);
        InvoiceItem invoiceItem2 = new InvoiceItem("2", "pizza", 2, 10000);
        System.out.println(invoiceItem1.toString());    
        System.out.println(invoiceItem2.toString());
        InvoiceArray.add(invoiceItem1);
        InvoiceArray.add(invoiceItem2);

        return InvoiceArray;
    }
}
